import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';
import { FormsModule }   from '@angular/forms'; // <-- NgModel lives here
import { ItemDetailComponent } from './itemDetail.component.js';
import { ItemService } from './item.service';
//import { OrderBy } from './sortPipe';

@NgModule({
  imports:[
    BrowserModule,
    FormsModule
  ],
  declarations: [
    AppComponent,
    ItemDetailComponent
  //  OrderBy
  ],
  providers: [ 
    ItemService 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

