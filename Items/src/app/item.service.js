"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var mockItems_1 = require("./mockItems");
var ItemService = (function () {
    function ItemService() {
        this.i = 0;
    }
    ItemService.prototype.getItems = function () {
        var resultArr = [];
        for (var j = 0; j < 3; j++) {
            resultArr[j] = mockItems_1.ITEMS[this.i];
            this.i++;
        }
        if (this.i == 9) {
            this.i = 0;
        }
        return resultArr;
    };
    return ItemService;
}());
ItemService = __decorate([
    core_1.Injectable()
], ItemService);
exports.ItemService = ItemService;
//# sourceMappingURL=item.service.js.map