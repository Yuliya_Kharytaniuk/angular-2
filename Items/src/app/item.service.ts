import { Injectable } from '@angular/core';

import { Item } from './Item';
import { ITEMS } from './mockItems';

@Injectable()
export class ItemService {
	 i=0;
	 getItems(): Item[] {
	 	var resultArr=[];
	 	for( var j=0;j<3;j++){
           resultArr[j]=ITEMS[this.i]
           this.i++;
		}

		 if(this.i==9){
	 		this.i=0;
		 }
	 	
	 	return resultArr;
	 }
}
