/**
 * Created by Nevie on 4/17/2017.
 */
export class Item {
    id: number;
    name: string;
    data: number;
    type: string;
}