import { Item } from './Item';

export const ITEMS: Item[] = [
    {
        id: 0,
        name: 'Other',
        data: 6,
        type: '%'
    },
    {
        id: 1,
        name: 'Defendant',
        data: 22,
        type: '%'
    },
    {

        id: 2,
        name: 'Plaintiff',
        data: 72,
        type: '%'
    }, 
    {
        id: 31,
        name: 'Other1',
        data: 61,
        type: '%'
    },
    {
        id: 42,
        name: 'Defendant1',
        data: 21,
        type: '%'
    },
    {

        id: 5,
        name: 'Plaintiff1',
        data: 43,
        type: '%'
    },
     {
        id:6,
        name: 'Other2',
        data: 66,
        type: '%'
    },
    {
        id: 7,
        name: 'Defendant2',
        data: 29,
        type: '%'
    },
    {

        id: 8,
        name: 'Plaintiff2',
        data: 45,
        type: '%'
    }
];