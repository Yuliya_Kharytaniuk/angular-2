/**
 * Created by Nevie on 4/17/2017.
 */
import { Component, EventEmitter, Input, Output} from '@angular/core';
import { Item } from './Item';

@Component({
    selector: 'item-detail',

    template:
        `  <div *ngIf="item" (click)="change(true)">
            <span >{{item.data}}</span >
            <label>{{item.type}}</label>
            <span >{{item.name}}</span > 
          </div>`
})
export class ItemDetailComponent {
      @Input() item: Item;
      @Output() onChanged = new EventEmitter<boolean>();
    change(increased) {
        this.onChanged.emit(increased);
    }
}