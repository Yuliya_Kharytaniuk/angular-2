"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by Nevie on 4/17/2017.
 */
var core_1 = require("@angular/core");
var Item_1 = require("./Item");
var ItemDetailComponent = (function () {
    function ItemDetailComponent() {
        this.onChanged = new core_1.EventEmitter();
    }
    ItemDetailComponent.prototype.change = function (increased) {
        this.onChanged.emit(increased);
    };
    return ItemDetailComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Item_1.Item)
], ItemDetailComponent.prototype, "item", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ItemDetailComponent.prototype, "onChanged", void 0);
ItemDetailComponent = __decorate([
    core_1.Component({
        selector: 'item-detail',
        template: "  <div *ngIf=\"item\" (click)=\"change(true)\">\n            <span >{{item.data}}</span >\n            <label>{{item.type}}</label>\n            <span >{{item.name}}</span > \n          </div>"
    })
], ItemDetailComponent);
exports.ItemDetailComponent = ItemDetailComponent;
//# sourceMappingURL=itemDetail.component.js.map