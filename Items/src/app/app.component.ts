import { Component } from '@angular/core';

import { Item } from './Item';
import { ItemService } from './item.service';
//import { OrderBy } from './sortPipe';



@Component({
    selector: 'my-app',
    templateUrl: './app.component.html'

})
export class AppComponent {
    title = 'Angular';
    items : Item[];
    constructor(private itemService: ItemService) {         
     }

    getItems(): void {
      this.items= this.itemService.getItems();
    }

    ngOnInit(): void {
        this.getItems();
    }

    onChanged(increased){
      if(increased==true){
        this.getItems();
      }
      
    }

    onSelect(item: Item): void {
      this.getItems();
    }
}