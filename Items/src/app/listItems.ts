/**
 * Created by Nevie on 4/17/2017.
 */
import { Component, EventEmitter, Input, Output} from '@angular/core';
import { Item } from './Item';

@Component({
    selector: 'list-items',

    template:'./listItems.html'
})
export class ItemDetailComponent {
      @Input() item: Item;
      @Output() onChanged = new EventEmitter<boolean>();
    change(increased) {
        this.onChanged.emit(increased);
    }
}